<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);

$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\Api\V1\Controllers\SignUpController@signUp');
        $api->post('confirmation', 'App\Api\V1\Controllers\SignUpController@confirmation');
        $api->post('login', 'App\Api\V1\Controllers\LoginController@login');

        $api->post('recovery', 'App\Api\V1\Controllers\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\Api\V1\Controllers\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\Api\V1\Controllers\LogoutController@logout');
        $api->post('refresh', 'App\Api\V1\Controllers\RefreshController@refresh');
        $api->post('me', 'App\Api\V1\Controllers\UserController@me');
    });

    $api->group(['prefix' => 'user', 'namespace' => 'App\Api\V1\Controllers\User'],  function(Router $api) {
        $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
            $api->get('get', 'UserController@get');
            $api->get('get-id', 'UserController@getById');
            $api->post('create', 'UserController@create');
            $api->post('update', 'UserController@update');
            $api->post('delete', 'UserController@delete');
            $api->post('confirm', 'UserController@confirm');
        });

        $api->group(['prefix' => 'client'], function(Router $api) {
            $api->post('add', 'ClientController@add');
        });

        $api->group(['prefix' => 'permission'], function(Router $api) {
            $api->get('get', 'PermissionController@get');
            $api->post('create', 'PermissionController@create');
            $api->post('update', 'PermissionController@update');
            $api->post('delete', 'PermissionController@delete');
            $api->post('user-get', 'PermissionController@getUser');
            $api->post('user-create', 'PermissionController@createUser');
            $api->post('user-update', 'PermissionController@updateUser');
        });

        $api->group(['prefix' => 'role'], function(Router $api) {
            $api->get('get', 'RoleController@get');
            $api->get('get-permission', 'RoleController@getPermission');
            $api->post('create', 'RoleController@create');
            $api->post('update', 'RoleController@update');
            $api->post('delete', 'RoleController@delete');
            $api->post('assign-permission', 'RoleController@assignPermission');
            $api->post('user-get', 'RoleController@getUser');
            $api->post('user-create', 'RoleController@createUser');
            $api->post('user-update', 'RoleController@updateUser');
        });
    });
    
    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->post('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->post('check-token', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});
