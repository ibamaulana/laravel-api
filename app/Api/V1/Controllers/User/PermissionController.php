<?php

namespace App\Api\V1\Controllers\User;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class PermissionController extends Controller
{
    public function get(JWTAuth $JWTAuth)
    {   
        $permission = Permission::get(); 

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
            'data' => $permission
        ], 200);
    }

    public function create(Request $request, JWTAuth $JWTAuth)
    {   
        $permission = Permission::create(['name' => $request->permission_name, 'guard_name' => 'web']);

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function delete(Request $request, JWTAuth $JWTAuth)
    {   
        $permission = Permission::findOrFail($request->id); 

        if(!$permission->delete()) {
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function createUser(Request $request, JWTAuth $JWTAuth)
    {   
        $user = User::where('email','=',$request->email)->first();

        $assign = $user->givePermissionTo($request->permission_name);
        
        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function updateUser(Request $request, JWTAuth $JWTAuth)
    {   
        $user = User::find($request->id);

        $assign = $user->syncPermissions($request->permissions);

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function getUser(Request $request, JWTAuth $JWTAuth)
    {   
        $user = User::find($request->id);

        $assign = $user->getAllPermissions();

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
            'data' => $assign
        ], 200);
    }
 
}
