<?php

namespace App\Api\V1\Controllers\User;

use Config;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Mail;

class UserController extends Controller
{
    public function get()
    {
        $user = User::role(['user','admin','superadmin'])->with('roles')->paginate(10);

         return response()->json([
            'status' => 'ok',
            'status_code' => 200,
            'data' => $user
        ], 200);
    }

    public function getById(Request $request)
    {
        $user = User::where('id', '=', $request->id)->with(['roles','permissions'])->first();
        $role = $user->getRoleNames()->toArray();

        $permission = $user->getAllPermissions()->toArray();

         return response()->json([
            'status' => 'ok',
            'status_code' => 200,
            'data' => $user,
            'role' => $role,
            'permission' => $permission,
        ], 200);
    }

    public function create(Request $request, JWTAuth $JWTAuth)
    {   
        $user = new User($request->only('name','email','password'));
        if(!$user->save()) {
            throw new HttpException(500);
        }

        $role = $user->assignRole($request->role);
        $permission = $user->givePermissionTo($request->permission);  

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 201);
    }

    public function update(Request $request, JWTAuth $JWTAuth)
    {   
        $id = $request->id;
        $user = User::where('id', '=', $id)->update($request->only('name','email','confirm'));
        if(!$user) {
            throw new HttpException(500);
        }

        $datauser = User::where('id', '=', $id)->first();
        $role = $datauser->syncRoles($request->role);
        $permission = $datauser->givePermissionTo($request->permission);  

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 201);
    }

    public function confirmation(Request $request)
    {
        $id = $request->id;

        $confirm = User::where('id', '=', $id)->update(['confirm' => 1]);

        if(!$confirm){
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 201);
    }

    public function delete(Request $request, JWTAuth $JWTAuth)
    {   
        $id = $request->id;
        $user = User::findOrFail($id);

        if(!empty($user)){
            $user->delete();
            $response = [
                'status' => 'Success delete data',
                'status_code' => '200'
            ];
        } else {
            $response = [
                'status' => 'Data not found',
                'status_code' => '400'
            ];
        }

        return response()->json($response, 201);
    }
}
