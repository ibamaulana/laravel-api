<?php

namespace App\Api\V1\Controllers\User;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class RoleController extends Controller
{
    public function get(JWTAuth $JWTAuth)
    {   
        $role = Role::with(['permissions'])->get(); 

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
            'data' => $role
        ], 200);
    }

    public function getPermission(Request $request, JWTAuth $JWTAuth)
    {   
        $role = Role::where('id','=',$request->id)->with(['permissions'])->get(); 

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
            'data' => $role
        ], 200);
    }

    public function assignPermission(Request $request, JWTAuth $JWTAuth)
    {   
        $role = Role::findByName($request->role_name, 'web');
        $role->syncPermissions($request->permission_name);

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function create(Request $request, JWTAuth $JWTAuth)
    {   
        $role = Role::create(['name' => $request->role_name,'guard_name' => 'web']);

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function update(Request $request, JWTAuth $JWTAuth)
    {   
        $role = Role::where('id','=', $request->id)->update(['name' => $request->role_name]);

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function delete(Request $request, JWTAuth $JWTAuth)
    {   
        $role = Role::findOrFail($request->id); 

        if(!$role->delete()) {
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function createUser(Request $request, JWTAuth $JWTAuth)
    {   
        $user = User::where('email','=',$request->email)->first();

        $assign = $user->syncRoles($request->role_name);

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function updateUser(Request $request, JWTAuth $JWTAuth)
    {   
        $user = User::find($request->id);

        $assign = $user->syncRoles($request->roles);

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    public function getUser(Request $request, JWTAuth $JWTAuth)
    {   
        $user = User::find($request->id);

        $assign = $user->getRoleNames();

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
            'data' => $assign
        ], 200);
    }

    
    
}
