<?php

namespace App\Api\V1\Controllers\User;

use Config;
use Auth;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ClientController extends Controller
{
    public function add(Request $request, JWTAuth $JWTAuth)
    {   
        $request->request->add(['password'=>'secret']);

        $user = new User($request->all());
        if(!$user->save()) {
            throw new HttpException(500);
        }
        
        $token = Auth::guard()->setTTL(null)->login($user);

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
            'token' => $token
        ], 201);
    }
}
