<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class LoginController extends Controller
{
    /**
     * Log the user in
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {   
        
        $user = User::where('email', $request->email)->first();

        if ($user == null) {
            return response()
                ->json([
                    'error' => [
                        'message' => 'Email does not exist ! Please fill with your registered email.',
                        'status_code' => 403
                    ]
                ],403);
        }else{
            $credentials = $request->only(['email', 'password']);

            try {
                $token = Auth::guard()->setTTL(60)->attempt($credentials);

                if(!$token) {
                     return response()
                        ->json([
                            'error' => [
                                'message' => 'Password does not match ! Please make sure your password is correct',
                                'status_code' => 403
                            ]
                        ],403);
                }

            } catch (JWTException $e) {
                throw new HttpException(500);
            }

            $confirm = $this->checkConfirmation($request->email);
            if ($confirm == 0) {
                return response()
                    ->json([
                        'error' => [
                            'message' => 'Your account is not confirmed ! Please confirm your account to complete your registration',
                            'status_code' => 403
                        ]
                    ],403);
            }else{
                $user = User::where('email', '=', $request->email)->first();

                $data = User::where('email', '=', $request->email)->first();

                $role = $data->getRoleNames()->toArray();

                $permission = $data->getAllPermissions()->toArray();

                return response()
                    ->json([
                        'status' => 'ok',
                        'status_code' => 200,
                        'token' => $token,
                        'data' => $user,
                        'role' => $role,
                        'permission' => $permission,
                        'expires_in' => Auth::guard()->factory()->getTTL() * 60
                    ]);
            }
        }

       
    }

   public function checkConfirmation($request)
   {
       $user = User::where(['email' => $request, 'confirm' => 1])->count();

       return $user;
   }
}
