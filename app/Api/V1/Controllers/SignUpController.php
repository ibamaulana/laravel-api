<?php

namespace App\Api\V1\Controllers;

use Config;
use App\User;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Api\V1\Requests\SignUpRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Support\Facades\Mail;
use App\Mail\AccountConfirmation;

class SignUpController extends Controller
{
    public function signUp(Request $request, JWTAuth $JWTAuth)
    {   
        $user = new User($request->all());
        if(!$user->save()) {
            throw new HttpException(500);
        }
        
        $detailuser = User::where('email','=', $request->email)->first();   

        $data = [
            'data' => $detailuser,
            'callback_url' => $request->callback_url
        ];

        $sendingResponse = Mail::to($request->email)->send(new AccountConfirmation($data));

        if(!Config::get('boilerplate.sign_up.release_token')) {
            return response()->json([
                'status' => 'ok',
                'status_code' => 200,
            ], 201);
        }

        $token = $JWTAuth->fromUser($user);
        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
            'token' => $token
        ], 201);
    }

    public function confirmation(Request $request)
    {
        $id = $request->id;

        $confirm = User::where('id', '=', $id)->update(['confirm' => 1]);

        if(!$confirm){
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 201);
    }
}
