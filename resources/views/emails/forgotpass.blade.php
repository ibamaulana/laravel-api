<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>CMS JVT | Forgot Password</title>
</head>
<body style="margin:0px; background: #f8f8f8; ">
<div width="100%" style="background: #f8f8f8; padding: 0px 0px; font-family:arial; line-height:28px; height:100%;  width: 100%; color: #514d6a;">
  <div style="max-width: 700px; padding:20px 0;  margin: 0px auto; font-size: 14px">
    <table border="0" cellpadding="0" cellspacing="0" style="width: 100%; margin-bottom: 20px">
      <tbody>
        <tr>
          <td style="vertical-align: top; padding-bottom:0px;" align="center"><a href="javascript:void(0)" target="_blank"><img src="{{ url('image/logo-jvt.png') }}" alt="Forget Password" style="border:none;height: 150px"><br/></a> 
			<b style="padding-top: 5px">Forgot Password</b>
          </td>
        </tr>
      </tbody>
    </table>
    <div style="padding: 40px; background: #fff;">
      <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
        <tbody>
          <tr>
            <td><b>Dear {{ $user->name }},</b>
              <p>Sorry to know that you forgot your account's password. Please click the button below for recovery your account.</p>
            </td>
          </tr>
          <tr>
            <td align="center">
              <a href="{{ $callback_url }}?token={{ $user->password }}" style="display: inline-block; padding: 11px 30px; margin: 20px 0px 30px; font-size: 15px; color: #fff; background: #1e88e5; border-radius: 60px; text-decoration:none;"> Reset Password</a>
            </td>
          </tr>
          <tr>
            <td>
              <p>Please click the button above then submit your new password.</p>
              <b>- Much Love, Ibam</b> </td>
          </tr>
        </tbody>
      </table>
    </div>
    <div style="text-align: center; font-size: 12px; color: #b2b2b5; margin-top: 20px">
      <p> Powered by Love </p>
    </div>
  </div>
</div>
</body>
</html>
