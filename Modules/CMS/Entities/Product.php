<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
	use SoftDeletes;

	/**
	* The attributes that use for name table
	*
	* @var string
	*/

	protected $table = 'product';

	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    	'prod_id',
    	'prod_name',
    	'prod_desc',
    	'prod_qty',
    	'prod_publish',
    	'prod_status',
    	'prod_image',
    ];

    /**
    * The attributes that used for softdeletes
    * 
    * @var array
    */

    protected $dates = ['deleted_at'];

    /**
    * Method for relation with \App\Model\Notaris
    *
    * @return array
    */
    // public function notaris()
    // {
    //     return $this->belongsTo(\App\Http\Model\Notaris::class);
    // }
}
