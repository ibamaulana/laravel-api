<?php

namespace Modules\CMS\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class WebProfile extends Model
{
	use SoftDeletes;

	/**
	* The attributes that use for name table
	*
	* @var string
	*/

	protected $table = 'web';

	 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $fillable = [
    	'id',
    	'web_name',
    	'web_description',
    	'web_tagline',
    	'web_email',
    	'web_phone',
        'web_address',
    ];

    /**
    * The attributes that used for softdeletes
    * 
    * @var array
    */

    protected $dates = ['deleted_at'];

    /**
    * Method for relation with \App\Model\Notaris
    *
    * @return array
    */
    // public function notaris()
    // {
    //     return $this->belongsTo(\App\Http\Model\Notaris::class);
    // }
}
