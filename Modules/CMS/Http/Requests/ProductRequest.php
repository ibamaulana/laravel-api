<?php

namespace Modules\CMS\Http\Requests;

use Config;
use Dingo\Api\Http\FormRequest;

class ProductRequest extends FormRequest
{
    public function rules()
    {
        return Config::get('cms.create.validation_rules');
    }

    public function authorize()
    {
        return true;
    }
}
