<?php
use Dingo\Api\Routing\Router;
Route::group(['middleware' => 'web', 'prefix' => 'cms', 'namespace' => 'Modules\CMS\Http\Controllers'], function()
{
    Route::get('/', 'CMSController@index'); 

	/** @var Router $api */
	$api = app(Router::class);

	$api->version('v1', function (Router $api) {
        $api->group(['prefix' => 'product', 'middleware' => 'jwt.auth', 'namespace' => 'Modules\CMS\Http\Controllers'], function(Router $api) {
        	$api->get('read', 'ProductController@read');
        	$api->post('create', 'ProductController@create');
        	$api->post('update/{id}', 'ProductController@update');
        	$api->post('delete/{id}', 'ProductController@destroy');
        	$api->post('get/{id}', 'ProductController@get');
	    });

	    $api->group(['prefix' => 'website', 'middleware' => 'jwt.auth', 'namespace' => 'Modules\CMS\Http\Controllers\Website'], function(Router $api) {
	    	$api->group(['prefix' => 'profil'], function(Router $api){
	    		$api->get('read', 'ProfilController@read');
	        	$api->post('create', 'ProfilController@create');
	        	$api->post('update/{id}', 'ProfilController@update');
	        	$api->post('delete/{id}', 'ProfilController@destroy');
	        	$api->post('get/{id}', 'ProfilController@get');
	    	});
        	// $api->get('read', 'ProductController@read');
        	// $api->post('create', 'ProductController@create');
        	// $api->post('update/{id}', 'ProductController@update');
        	// $api->post('delete/{id}', 'ProductController@destroy');
        	// $api->post('get/{id}', 'ProductController@get');
	    });

	    // $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
	    //     $api->post('protected', function() {
	    //         return response()->json([
	    //             'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
	    //         ]);
	    //     });

	    //     $api->get('refresh', [
	    //         'middleware' => 'jwt.refresh',
	    //         function() {
	    //             return response()->json([
	    //                 'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
	    //             ]);
	    //         }
	    //     ]);
	    // });
	});
});

