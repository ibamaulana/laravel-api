<?php

namespace Modules\CMS\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Tymon\JWTAuth\JWTAuth;
use Modules\CMS\Http\Requests\ProductRequest;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Config;
use Illuminate\Support\Facades\Storage;
use Modules\CMS\Entities\Product;


class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        Storage::put('file.txt', 'Contents');
        return response()->json(['message' => 'hai']);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create(ProductRequest $request)
    {   
        // $i=0;
        // foreach ($request['prod_image'] as $key) {
        //     $i++;
        //     $file = base64_decode($key);
        //     Storage::put($request['prod_id'].'-'.$i.'.jpg', $file);
        // }
        // $file = $file->file('prod_image');
        // die(print_r($request->all()));
        if ($request['prod_image'] != null) {
            $file = base64_decode($request['prod_image']);
            $filename = $request['prod_id'].'.jpg';

            $upload = Storage::disk('image')->put($filename, $file);
            
            $path = url('storage/image/'.$filename);
        }else{
            $path = url('storage/image/no-photo.jpg');
        }
        

        $datas = [
            'prod_id' => $request['prod_id'],
            'prod_name' => $request['prod_name'],
            'prod_desc' => $request['prod_desc'],
            'prod_qty' => $request['prod_qty'],
            'prod_publish' => $request['prod_publish'],
            'prod_status' => $request['prod_status'],
            'prod_image' => $path
        ];

        $product = new Product($datas);

        if(!$product->save()) {
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok',
            'status_code' => 200,
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function read()
    {
        $data = Product::paginate(10);

        return response()->json([
            'status_code' => 200,
            'data' => $data
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('cms::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update($id, ProductRequest $request)
    {
        // dd($request->all());
        if ($request['prod_image'] != null) {
            $file = base64_decode($request['prod_image']);
            $filename = $request['prod_id'].'.jpg';

            $upload = Storage::disk('image')->put($filename, $file);
            
            $path = url('storage/image/'.$filename);

            $data = [
                'prod_id' => $request['prod_id'],
                'prod_name' => $request['prod_name'],
                'prod_desc' => $request['prod_desc'],
                'prod_qty' => $request['prod_qty'],
                'prod_publish' => $request['prod_publish'],
                'prod_status' => $request['prod_status'],
                'prod_image' => $path
            ];
        }else{
            $data = [
                'prod_id' => $request['prod_id'],
                'prod_name' => $request['prod_name'],
                'prod_desc' => $request['prod_desc'],
                'prod_qty' => $request['prod_qty'],
                'prod_publish' => $request['prod_publish'],
                'prod_status' => $request['prod_status'],
            ];
        }

        $model = Product::where('prod_id','=',$id)
        ->update($data);

        if($model != 1) {
            throw new HttpException(500);
        }

        return response()->json([
            'status' => 'ok',
            'status_code' => 200
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy($id)
    {
        $model = Product::where('prod_id','=',$id);
        if(!empty($model)){
            $model->forcedelete(); //use $model->delete() for Soft Delete
            $response = [
                'status' => 'Success delete data',
                'status_code' => 200
            ];
        } else {
            $response = [
                'status' => 'Data not found',
                'status_code' => 400
            ];
        }

        return Response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function get($id)
    {
        $data = Product::where('prod_id','=',$id)->get();

        return response()->json([
            'status_code' => 200,
            'data' => $data
        ], 200);
    }
}
