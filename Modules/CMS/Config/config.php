<?php

return [
    'name' => 'CMS',
    'create' => [

        // here you can specify some validation rules for your sign-in request
        'validation_rules' => [
            'prod_id' => 'required',
            'prod_name' => 'required',
        ]
    ]
];
